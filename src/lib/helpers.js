function getParam(name){
  var u = new URL(window.location.href)
  return u.searchParams.get(name);
}
function _getUrlWithParam(url, name, value){
  var u = new URL(url)
  var params = u.searchParams;
  if (params.has(name)){
    params.delete(name);
  }
  params.append(name, value);
  return `${u.pathname}?${params.toString()}${u.hash}`;
}
function getUrlWithParam(name, value){
  return _getUrlWithParam(window.location.href, name, value);
}
function sanitizeInt(val, _default){
  if (!val){
    return _default;
  }
  try{
    val = parseInt(val, 10);
    if (isNaN(val)){
      return _default;
    }
    if (val < _default){
      return _default;
    }
    return val;
  }catch(e){
    return _default;
  }
}
function constructApiUrlWithParams({api_url, page, search}){
  let _url = new URL(api_url);
  if (page){
    _url.searchParams.append('page',page);
  }
  if (search){
    _url.searchParams.append('search',search);
  }
  return _url.toString();
}
function getListUrlWithParam(page, search){
  if (search){
    return `/list/${page}/${search}`;
  }else{
    return `/list/${page}`;
  }
  
}
export {
  getParam,
  sanitizeInt,
  getUrlWithParam,
  _getUrlWithParam,
  constructApiUrlWithParams,
  getListUrlWithParam
}