import { sanitizeInt, _getUrlWithParam } from "./helpers";

it('properly sanitize integer', () => {
  expect(sanitizeInt(10,1)).toBe(10)
  expect(sanitizeInt(-1,1)).toBe(1)
  expect(sanitizeInt(null,1)).toBe(1)
  expect(sanitizeInt("party",1)).toBe(1)
})
it('generates proper url', () => {
  expect(_getUrlWithParam('http://localhost:3000/?page=2#/list/','page',3)).toBe('/?page=3#/list/');
  expect(_getUrlWithParam('http://localhost:3000/#/list/','page',3)).toBe('/?page=3#/list/');
})