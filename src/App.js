import React, { Component } from 'react';
import Header from './components/header/index';
import Form from './routes/form/index';
import List from './routes/list/index';
import About from './routes/about/index'

import './style/index.css';
import { Switch, Route } from 'react-router-dom'

class App extends Component {
  render() {
    return (
      <div>
        <Header />
        <Switch>
          <Route path='/list/:page/:search' component={List}/>
          <Route path='/list/:page' component={List}/>
          <Route path='/list' component={List}/> 
          
          
          <Route path='/about/:id' component={About}/>
          <Route path='/' component={Form}/>
        </Switch>

      </div>
    );
  }
}

export default App;
