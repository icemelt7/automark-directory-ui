import React, { Component } from 'react';
import Helmet from 'react-helmet';
import {
    FacebookShareButton,
    TwitterShareButton,
    WhatsappShareButton,
  } from 'react-share';
import { CONST } from '../../constants';
  


export default class About extends Component {
    state = {
        profiles: " "
      }
    componentDidMount(){
        fetch(CONST.WP_API+`/profile/?slug=${this.props.match.params.id}`).then(r => {
          r.json().then(profiles => {
            
            this.setState({profiles: profiles[0]});
          })
          
        });
        
      }
    render() {
        const placeholder = 'https://vignette.wikia.nocookie.net/citrus/images/6/60/No_Image_Available.png/revision/latest?cb=20170129011325';
        return <div>
                {
                (this.state.profiles.acf==null) ? <div className="d-flex justify-content-center"><h1>Loading</h1></div> :                        
                <div style={{marginTop: "20px"}}>
                <Helmet>
                <title>{this.state.profiles.acf.name} - Linkbook</title>
                <meta name="description" content={`${this.state.profiles.acf.name} - ${this.state.profiles.acf.summary}`}/>
                {this.state.profiles.acf.picture && <meta property="og:image" content={this.state.profiles.acf.picture} /> }
                </Helmet>
                    <div className="profile-page">
                    <div className="wrapper">
                        <div className="page-header page-header-small" filter-color="green">
                        <div className="page-header-image" data-parallax="true" style={{backgroundImage: "url('images/cc-bg-1.jpg')"}}></div>
                        <div className="container">
                            <div className="content-center">
                            <div className="cc-profile-image"><a href="profile-image"><img onError={(e) => e.target.src = placeholder}
                      src={ ( this.state.profiles.acf.picture) ? this.state.profiles.acf.picture : placeholder} 
                      alt="pic"  /></a></div>
                            <div className="h2 title" style={{marginBottom: "50px"}}>{this.state.profiles.acf.name}</div>
                            <p className="category text-black">{this.state.profiles.acf.current_job_desc} at {this.state.profiles.acf.current_job_company}</p>
                            </div>
                        </div>
                        <div className="section">
                            <div className="container">
                                <div className="button-container">
                                    <a className="btn btn-default btn-round btn-lg btn-icon" href="facebookshare" rel="tooltip" title="Share on Facebook">
                                        <FacebookShareButton quote="Check out my Linkbook profile" hashtag="#Automark #Magazine" children={<i className="fa fa-facebook"></i>} url={window.location.href} />
                                    </a>
                                    <a className="btn btn-default btn-round btn-lg btn-icon" href="dummy" rel="tooltip" title="Share on Whatsapp">
                                        <WhatsappShareButton children={<i className="fa fa-whatsapp"></i>} url={window.location.href} />
                                    </a>
                                    <a className="btn btn-default btn-round btn-lg btn-icon" href="twittershare" rel="tooltip" title="Share on Twitter">
                                        <TwitterShareButton children={<i className="fa fa-twitter"></i>} url={window.location.href} />
                                    </a>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div className="section" id="about">
                    <div className="container">
                        <div className="card" data-aos="fade-up" data-aos-offset="10">
                        <div className="row">
                            <div className="col-lg-6 col-md-12">
                            <div className="card-body">
                                <div className="h4 mt-0 title">About</div>
                                {(this.state.profiles.acf.summary==="") ? <p className="font-italic">No summary</p> : 
                                <p dangerouslySetInnerHTML={{__html: this.state.profiles.acf.summary}} />
                                }
                            </div>
                            </div>
                            <div className="col-lg-6 col-md-12">
                            <div className="card-body">
                                <div className="h4 mt-0 title">Basic Information</div>
                                <div className="row mt-3">
                                <div className="col-sm-4"><strong className="text-uppercase">Information:</strong></div>
                                <div className="col-sm-8">{this.state.profiles.acf.current_job_desc} at {this.state.profiles.acf.current_job_company}</div>
                                </div>
                                <div className="row mt-3">
                                <div className="col-sm-4"><strong className="text-uppercase">Headline:</strong></div>
                                <div className="col-sm-8">{this.state.profiles.acf.headline}</div>
                                </div>
                                <div className="row mt-3">
                                <div className="col-sm-4"><strong className="text-uppercase">Email:</strong></div>
                                <div className="col-sm-8">{this.state.profiles.acf.email}</div>
                                </div>
                                <div className="row mt-3">
                                <div className="col-sm-4"><strong className="text-uppercase">Phone:</strong></div>
                                <div className="col-sm-8">{this.state.profiles.acf.phone_number}</div>
                                </div>
                                <div className="row mt-3">
                                <div className="col-sm-4"><strong className="text-uppercase">Location:</strong></div>
                                <div className="col-sm-8">{this.state.profiles.acf.location}</div>
                                </div>
                            </div>
                            </div>
                        </div>
                        </div>
                        <h4 style={{padding: "20px", marginTop: "20px", backgroundColor: "wheat"}}>Create your <a href="/linkbook/" style={{textDecoration: "underline", color: "blue"}}>Own profile </a></h4>
                    </div>
                    </div>
                    
                    </div>
                }
            
        </div>
            
    }

} 
