import React from 'react';
import ReactDOM from 'react-dom';
import { InstantSearch, Hits, SearchBox, Highlight, RefinementList, Pagination,
  CurrentRefinements,
  ClearRefinements } from 'react-instantsearch-dom';

const Algolia = () => (
  <InstantSearch
    appId="latency"
    apiKey="3d9875e51fbd20c7754e65422f7ce5e1"
    indexName="bestbuy"
  >
    <Search />
  </InstantSearch>
);
function Search() {
  return (
    <div>
      <CurrentRefinements />
      <ClearRefinements />
      <SearchBox />
      <RefinementList attribute="category" />
      <Hits hitComponent={Product} />
      <Pagination />
    </div>
  );
}
function Product({ hit }) {
  return <div style={{ marginTop: '10px' }}>
  <span className="hit-name">
    <Highlight attribute="name" hit={hit} />
  </span>
</div>;
}

// Needed only if your js app doesn't do it already.
// Create-react-app does it for you
export default Algolia;
