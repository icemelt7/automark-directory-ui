import React, { Component } from 'react';
import logo from './logo.svg';
import './index.css'
import { Link } from 'react-router-dom';
import Helmet from 'react-helmet';
import { sanitizeInt, constructApiUrlWithParams, getListUrlWithParam } from '../../lib/helpers';
import { withRouter } from 'react-router';
import { CONST } from '../../constants';


//props.profile.acf.current_job_company
//props.profile.acf.name
//props.profile.acf.picture

//test
const GoBackBtn = (props) => {
  return <button className="btn btn-primary" onClick = {props.refresh}>Go back</button>
}
const Overlay = (props) => {
  return <div id="overlay" style={{display:`${props.display}`}}>
          <div id="text">Loading<img style={{paddingLeft: '50px'}} src={logo} alt="Loading" /></div>
        </div>
}
const Profile = (props) => {
  const placeholder = 'https://vignette.wikia.nocookie.net/citrus/images/6/60/No_Image_Available.png/revision/latest?cb=20170129011325';
  return  <div className="col-sm-6 profile">
  <Link to={`/about/${props.profile.slug}`}>
      <div className="panel">
          <div className="panel-body p-t-10">
              <div className="media-main">
                  <div className="pull-left">
                      <img className="thumb-lg img-circle bx-s" onError={(e) => e.target.src = placeholder}
                      src={ ( props.profile.acf.picture) ? props.profile.acf.picture : placeholder} 
                      alt="pic" />
                  </div>
                  <div className="info">
                      <h2>{props.profile.acf.name.toLowerCase()}</h2>
                      <p className="text-muted">{props.profile.acf.current_job_desc} at {props.profile.acf.current_job_company}</p>
                  </div>
              </div>
              <div className="clearfix"></div>
              
          </div>
      </div>
      </Link>
  </div>
 

}


class List extends Component {
  state = {
    profiles: [],
    inputValue: '',
    itemsCountPerPage: 0,
    totalItemsCount: 0,
    loading: true,
    comp_loading: true,
    display: 'none',
    isEmpty: false,
    pageloading: false,
    count: 5,
    workFunc: false,
    activePageDefault: 1,
  }
  handleProps = () => {
    const page = sanitizeInt(this.props.match.params.page,1);
    const search = this.props.match.params.search;
    this.setState({
      loading: true,
      inputValue: search ? search : ''
    })
    const api_url = `${CONST.WP_API}/profile`;
    const url_to_fetch = constructApiUrlWithParams({api_url, page, search});
    fetch(url_to_fetch).then(r => {
      this.setState({totalItemsCount: r.headers.get('X-WP-Total')})
      r.json().then(profiles => {
        this.setState({
          profiles: profiles,
          itemsCountPerPage: profiles.length,
          comp_loading: false,
          loading: false,
          workFunc: false,
        });
      })  
    });
  }
  componentDidUpdate(prevProps) {
    const pp = prevProps.match.params;
    const p = this.props.match.params;
    if ((pp.page !== p.page) || 
        (pp.search !== p.search)){
      this.handleProps();
    }
  }
  componentDidMount(){
    this.handleProps();
  }
  update = (e) => {
    this.setState({
      inputValue: e.target.value,
    })
  }

  work = (e) => {
   e.preventDefault()
   const search = this.state.inputValue;
   const pageNumber = 1;
   this.props.history.push(getListUrlWithParam(pageNumber, search));
  }
  handlePageChange = (pageNumber) => {
    const search = this.props.match.params.search;
    this.props.history.push(getListUrlWithParam(pageNumber, search));
  }
  render(){
    
    return <div className="container" style={{marginTop: "20px"}}>
            <Helmet>
            <title>Automark Professional Directory - Linkbook</title>
            <meta name="description" content="Industry list of top professionals in Pakistan, premium profiles of all talented people"/>
            </Helmet>
            <div className="row">
            <div className="col center">
              <h1>List of professional profiles on Automark Linkbook</h1>
            </div>
          </div>
            <div className="row">
              <form className = " col-md-6 offset-md-3" onSubmit = {this.work}>
                <div className = "form-group ">
                  <input type = "text"  value = {this.state.inputValue} className = "form-control " onChange = {this.update} id = "formGroupExampleInput" placeholder = "Search" />
                  <input type = "submit" className=" col-md-6 offset-md-3"value="Submit" />
                </div> 
            </form>
            </div>
            
            <div className="row">
              {this.state.comp_loading ? <div className = " col-md-6 offset-md-5"><h1>Loading</h1>
              <img style={{paddingLeft: '60px'}} src={logo} alt="Loading" /></div> : <div>{this.state.loading ? <Overlay display='block' /> : <Overlay display='none' />}</div>} 
              {this.state.isEmpty ?
                 <div  className = " col-md-6 offset-md-4"><h1>No Results found!</h1>
                  <button className="btn btn-primary col-md-6 offset-md-1" onClick = {this.refresh}> Go Back </button>
                 </div>
              : this.state.profiles.map(profile => <Profile key={profile.id} profile={profile} />)}
                  
            </div>
            <div className="row">
            <Pagination
                search={this.props.match.params.search}
                activePage={sanitizeInt(parseInt(this.props.match.params.page, 10),1)}
                itemsCountPerPage={this.state.itemsCountPerPage}
                totalItemsCount={this.state.totalItemsCount}
                onChange={this.handlePageChange}
            />
            {this.state.workFunc ? <GoBackBtn refresh={this.refresh} /> : null}  
            </div>
          </div>

  }
}
const Pagination = (props) => {
  const { search, 
      activePage,
      totalItemsCount } = props;
      const pages = Math.ceil(totalItemsCount/10)
  let _arr = [];
  var prevPageText = "⟨"
  var firstPageText = "«"
  var nextPageText =  "⟩"
  var lastPageText =  "»"
  _arr.push(<li key="a1"><Link className="page-link" to={getListUrlWithParam(1, search)}>{firstPageText}</Link></li>);
  _arr.push(<li key="a2"><Link className="page-link" to={getListUrlWithParam((activePage<=1 ? 1 : activePage-1), search)}>{prevPageText}</Link></li>);
  for (let i = 1; i <= pages; i++){
    _arr.push(<li key={i} className={(i === (activePage===null ? 1 : activePage)) ? 'page-item active' : ''}><Link className="page-link" to={getListUrlWithParam(i, search)}>{i}</Link></li>)
  }
  _arr.push(<li key="a3"><Link className="page-link" to={getListUrlWithParam((activePage>=pages ? pages : activePage+1), search)}>{nextPageText}</Link></li>);
  _arr.push(<li key="a4"><Link className="page-link" to={getListUrlWithParam(pages, search)}>{lastPageText}</Link></li>);
  return <ul className="pagination">
    {_arr}
  </ul>
}
export default withRouter(List)